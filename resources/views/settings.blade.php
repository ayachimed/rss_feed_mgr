@extends('layouts.app')

@section('content')
<div class="container-fluid">
    
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading text-center"><strong><i class="fa fa-cog" aria-hidden="true"></i> &nbsp;Settings</strong></div>

                <div class="panel-body">
                    @if(Session::has('Error'))
                        <div class="alert alert-danger text-center" >
                            <h4><b>{{Session::get('Error')}}</b></h4>
                        </div>
                    @endif
                    @if(Session::has('Success'))
                        <div class="alert alert-success text-center" >
                            <h4><b>{{Session::get('Success')}}</b></h4>
                        </div>
                    @endif
                    <a href="#" data-toggle="modal" data-target="#change_password_modal" class="btn btn-danger"><b>update password</b></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="change_password_modal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title">Update credentials</h3>
        </div>
        <div class="modal-body">
          <form class="" action="{{route('update_admin_password')}}" id="change_pass_form" method="post">
            {{csrf_field()}}
            <div class="form-group ">
                <label class="control-label " for="old_password">Old password</label>
                <input type="password" class="form-control " name="old_password" id="old_password" required/>
            </div>
            <div class="form-group ">
                <label class="control-label " for="pass1">New password</label>
                <input type="password" class="form-control " name="pass1" id="pass1" required/>
            </div>
            <div class="form-group ">
                <label class="control-label " for="pass2">Retype new password</label>
                <input type="password" class="form-control " name="pass2" id="pass2" required/>
            </div>
            <div class="form-group">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-default">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
</div>
@endsection
