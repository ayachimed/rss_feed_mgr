@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading text-center"><strong><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Admin Panel</strong></div>
                <div class="panel-body">
                @if(Session::has('Error'))
                    <div class="alert alert-danger alert-dismissible">
                        {{Session::get('Error')}}
                        <span class="close" data-dismiss='alert'>&times;</span>
                    </div>
                @endif
                @if(Session::has('Success'))
                    <div class="alert alert-success alert-dismissible">
                        {{Session::get('Success')}}
                        <span class="close" data-dismiss='alert'>&times;</span>
                    </div>
                @endif
                    <div class="row">
                    	<div class="col-md-8 col-lg-8">
                    		<div class="panel panel-info">
                    			<div class=" panel-heading"><b>Feed Urls<span class="pull-right"><a href="#" data-toggle="modal" data-target="#new_url_modal">Add</a></span></b></div>
                    			<div class=" panel-body">
                    				<table class="table table-striped table-hover">
                    					<thead>
                    						<tr>
                    							<th>URL</th>
                    							<th>Category</th>
                                                <th>Action</th>
                    						</tr>
                    					</thead>
                    					<tbody>
                    						@foreach($urls as $url)
                                            <tr>
                                                <td><b>{{str_limit($url->url,40,'...')}}</b></td>
                                                <td><b>{{$url->category->name}}</b></td>
                                                <td><b><a href="{{route('show_update_url',['id'=>$url->id])}}">Update</a> | <a href="{{route('delete_url',['id'=>$url->id])}}" style="color:red">delete</a></b></td>
                                            </tr>
                    						@endforeach
                    					</tbody>
                    				</table>
                    			</div>
                    		</div>
                    	</div>
                    	<div class="col-md-4 col-lg-4">
                    		<div class="panel panel-danger">
                    			<div class="panel-heading"><b>Categories<span class="pull-right"><a href="#" data-toggle="modal" data-target="#new_category_modal">Add</a></span></b></div>
                    			<div class="panel-body">
                    				<ul class="list-group">
                                    @foreach($categories as $cat)
                                        <a  class="list-group-item" href="#">
                                            {{$cat->name}}
                                        </a>
                                    @endforeach
                    			</div>
                    		</div>
                    	</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="new_url_modal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Create a new Feed URL</h2>
            </div>
            <div class="modal-body">
                <form id="urlForm"method="post" action="{{route('new_url')}}"> 
                {{csrf_field()}}
                    <div class="form-group">
                        <label for="url">URL:</label>
                        <input type="text" name="url" id="url" class="form-control" required>
                    </div>
                    <div>
                        <label for="cat">Choose category:</label>
                        <select id="cat" name="category" class="form-control">
                            @foreach($categories as $c)
                            <option>{{$c->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer ">
                <div class="form-group">
                    <button class="btn btn-danger" onclick="$('#url').val()!=''?$('#urlForm').submit():$('#url').focus()">Submit</button>
                    <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="new_category_modal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Create a new FCategory</h2>
            </div>
            <div class="modal-body">
                <form id="catForm"method="post" action="{{route('new_category')}}"> 
                {{csrf_field()}}
                    <div class="form-group">
                        <label for="category">Category Name:</label>
                        <input type="text" name="category" id="category" class="form-control" required>
                    </div>
                </form>
            </div>
            <div class="modal-footer ">
                <div class="form-group">
                    <button class="btn btn-danger" onclick="$('#category').val()!=''?$('#catForm').submit():$('#category').focus()">Submit</button>
                    <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
