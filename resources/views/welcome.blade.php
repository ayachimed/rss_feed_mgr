@extends('layouts.app')

@section('content')
<div class="container">
	<div class="panel panel-primary">
		<div class="panel-heading text-center"><b>Feeds</b></div>
		<div class="panel-body">
			<center>
				@foreach($categories as $c)
				<a href="{{route('landing_page',['category_id'=>$c->id])}}" class="btn {{'btn-'.$arr[rand(0,count($arr)-1)]}}">{{$c->name}}</a>
				@endforeach
			</center>
			<br>
			<ul>
			@foreach($feeds as $feed)
				<li class="media" style="list-style:none">
                    <a href="#" class="pull-left">
                        <img src="{{$feed->image_link}}" alt="" class="img-circle img-responsive">
                    </a>
                    <div class="media-body">
                        <span class="text-muted pull-right">
                            <small class="text-muted">{{$feed->pubDate}}</small>
                        </span>
                     	<blockquote>
                         	<a href="#" class="badge" style="background:#3097D1">{{$feed->category->name}}</a>
	                        <a href="#" data-toggle="modal" data-target="#feed{{$loop->index}}" >{{$feed->title}}</a>
							<p>{{$feed->description}}</p>
							<a href="{{$feed->link}}" target="_blank">{{$feed->link}}</a>
						</blockquote>
                    </div>
                </li>
                <div class="modal fade" id="feed{{$loop->index}}" role="dialog">
				    <div class="modal-dialog modal-md">
				        <div class="modal-content">
				            <div class="modal-header">
				            	{{$feed->title}}
				            </div>
				            <div class="modal-body">
				            	<center><img src="{{$feed->image_link}}" class="img-responsive img-thumbnail" width="50%"></center>
				            	<p>{{$feed->description}}</p>
				            </div>
				            <div class="modal-footer ">
				            	<div class="form-grouppull-right">
				            		<button class="btn btn-default" data-dismiss="modal">Close</button>
				            		<a href="{{$feed->link}}" target="_blank" class="btn btn-primary">See More</a>
				            	</div>
				            </div>
				        </div>
				    </div>
				</div>
			@endforeach
			</ul>
			<center>
				{{$feeds->appends(Request::only('category_id'))->links()}}
			</center>
			<br>
		</div>
	</div>
</div>
@endsection