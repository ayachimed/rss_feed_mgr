@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        	<div class="panel panel-primary">
        		<div class="panel-heading"><b>Update Feed URL</b></div>
        		<div class="panel-body">
        			@if(Session::has('Error'))
	                    <div class="alert alert-danger alert-dismissible">
	                        {{Session::get('Error')}}
	                        <span class="close" data-dismiss='alert'>&times;</span>
	                    </div>
	                @endif
        			<form class="form-horizontal" id="" method="POST" action="{{route('update_url')}}">
        				{{csrf_field()}}
        				<input type="hidden" name="id" value="{{$url->id}}">
					  <div class="form-group">
					    <label class="control-label col-sm-2" for="URL">URL:</label>
					    <div class="col-sm-10">
					      <input type="text" class="form-control" value="{{$url->url}}" name="url" id="URL" placeholder="Enter URL" required>
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="control-label col-sm-2" for="category">Category:</label>
					    <div class="col-sm-10"> 
					      <select name='category' class="form-control" id="category">
					      	<option>Select...</option>
					      	@foreach($categories as $cat)
					      		<option>{{$cat->name}}</option>
					      	@endforeach
					      </select>
					    </div>
					  </div>
					  <div class="form-group"> 
					    <div class="col-sm-offset-2 col-sm-10">
					      <button type="submit" class="btn btn-success">Submit</button>
					      <a href="{{route('dashboard')}}" class="btn btn-danger">Cancel</a>
					    </div>
					  </div>
					</form>
        		</div>
        	</div>
        </div>
    </div>
</div>
@endsection