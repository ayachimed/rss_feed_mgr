<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class FeedUrlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        DB::table('feed_urls')->truncate();
        $urls = [
        	"url1" =>[
                'url' => 'http://rss.nytimes.com/services/xml/rss/nyt/World.xml',
                'category_id' => 1,
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s')
            ],
            "url2" =>[
                'url' => 'http://www.nytimes.com/services/xml/rss/nyt/PersonalTech.xml',
                'category_id' => 2,
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s')
            ],
            "url3" =>[
                'url' => 'http://www.nytimes.com/services/xml/rss/nyt/Politics.xml',
                'category_id' => 3,
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s')
            ],
            "url4" =>[
                'url' => 'http://www.nytimes.com/services/xml/rss/nyt/SmallBusiness.xml',
                'category_id' => 4,
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s')
            ],
            "url5" =>[
                'url' => 'http://www.nytimes.com/services/xml/rss/nyt/YourMoney.xml',
                'category_id' => 5,
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s')
            ],
            "url6" =>[
                'url' => 'http://www.nytimes.com/services/xml/rss/nyt/Economy.xml',
                'category_id' => 6,
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s')
            ],
            "url7" =>[
                'url' => 'http://www.nytimes.com/services/xml/rss/nyt/Environment.xml',
                'category_id' => 1,
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s')
            ],
            "url8" =>[
                'url' => 'http://www.nytimes.com/services/xml/rss/nyt/Science.xml',
                'category_id' => 2,
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s')
            ],
            "url9" =>[
                'url' => 'http://www.nytimes.com/services/xml/rss/nyt/Space.xml',
                'category_id' => 3,
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s')
            ],
            "url10" =>[
                'url' => 'http://feeds.nytimes.com/nyt/rss/Technology',
                'category_id' => 4,
                "created_at" => Carbon::now()->format('Y-m-d H:i:s'),
                "updated_at" => Carbon::now()->format('Y-m-d H:i:s')
            ],

            

        ];
        DB::table("feed_urls")->insert($urls);
    }
}
