$('#change_pass_form').on("submit",function(e){
    if($('#pass1').val() != $('#pass2').val()){
        e.preventDefault();
        alert('Passwords don\'t match, please try again')
        $('#pass1').focus();
    }
});