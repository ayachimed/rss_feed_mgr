<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\FeedUrl;
use Yadakhov\InsertOnDuplicateKey;
class Feed extends Model
{

	public function category()
	{
		return $this->belongsTo("App\Category");
	}
	use InsertOnDuplicateKey;
    public static function updateFeed()
    {
    	try {
    		$urls = FeedUrl::all();
	    	$feeds = [];
	    	\DB::table('feeds')->truncate();
	    	foreach($urls as $u){
	    		$xml = simplexml_load_file($u->url); 
	    		$items = $xml->xpath("//item[./media:content/@url]");
	    		echo ("fetched ". count($items)." items.\n");
	    		$images = $xml->xpath("//media:content/@url");
	    		$feed = [];
	    		for($i = 0;$i<count($items);$i++) {
	    			$feed['link'] = $items[$i]->link;
	    			$feed['description'] = $items[$i]->description;
	    			$feed['title'] = $items[$i]->title;
	    			$feed['pubDate'] = $items[$i]->pubDate;
	    			$feed['image_link'] = $images[$i]->url;
	    			$feed['category_id'] = $u->category_id;
	    			$feeds[] = $feed;
	    		}
	    	}

	    	Feed::replace($feeds,['title']);
	    	echo "Feed updated with a total of: ".count($feeds)." items";
    	} catch (Exception $e) {
    		echo "Error Occured try again Later";
    	}
    }
}
