<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\FeedUrl;
use App\Feed;
use App\Category;
use Session;
class FeedController extends Controller
{
    public function show_feeds_index(Request $r){
        $category_id = 1;
        if($r->get('category_id')){
            $category_id = $r->get('category_id');
        }

    	$feeds = Feed::where('category_id',$category_id)->orderBy('id','desc')->paginate(10);
        $categories = Category::all();
        $arr = ['danger','primary','warning','success'];
    	return view("welcome",["feeds"=>$feeds,"categories"=>$categories,"arr"=>$arr]);
    }

    public function post_url(Request $r){
        $newUrl = new FeedUrl();
        if($r->url && $r->category){
            $category_id = Category::where("name",$r->category)->value('id');
            if($category_id){
                $newUrl->url = $r->url;
                $newUrl->category_id = $category_id;
                $newUrl->save();
                Session::flash("Success","URL successfully added");
                return redirect()->route("dashboard");
            }
        }
        Session::flash("Error","Error Occured please try again");
        return redirect()->back();
        
    }

    public function delete_url($id)
    {
        $url = FeedUrl::find($id);
        if($url){
            if($url->delete()){
                Session::flash("Success","URL successfully deleted");
                return redirect()->back();
            }
            Session::flash("Error","Error while deleting URL! try again later");
            return redirect()->back();
        }
    }
    public function show_update_url_view($id){
        $feedUrl = FeedUrl::findOrFail($id);
        $categories = Category::all();
        if($feedUrl){
            return view('update_url_view',[
                "url" => $feedUrl,
                "categories" => $categories
            ]);
        }else{
            Session::flash("Error" ,"Error occured!");
            return redirect()->back();
        }
    }

    public function update_url(Request $r){
        if($r->id && $r->url && $r->category){
            $feedUrl = FeedUrl::find($r->id);
            if($feedUrl){
                $feedUrl->url = $r->url;
                $category_id = Category::where('name',$r->category)->value('id');
                if($category_id){
                    $feedUrl->category_id = $category_id;
                    $feedUrl->update();
                    Session::flash("Success","FeedUrl successfully Updated");
                    return redirect()->route('dashboard');
                }else{
                    Session::flash("Error","FeedUrl failed to Update, Choose another Category");
                }
            }else{
                Session::flash("Error","FeedUrl failed to Update, try again");
            }
        }else{
            Session::flash("Error","FeedUrl failed to Update, try again");
        }
        return redirect()->back();

    }
}