<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\User;
use Session;
class UserController extends Controller
{
    public function change_password(Request $r)
    {
    	$old_password = $r->old_password;
    	$pass1 = $r->pass1;
    	$pass2 = $r->pass2;
    	if($pass1 == $pass2){
    		
    		if (Auth::attempt(['email' => Auth::user()->email, 'password' => $old_password])) {
    			$user = User::find(Auth::user()->id);
    			$user->password = bcrypt($pass1);
    			$user->update();
    			Session::flash("Success","Password successfull updated");
    		}else{
    			Session::flash("Error","This is an invalid password");
    		}
    		return redirect()->back();
    	}else{
    		Session::flash("Error","Error, passwords don't match, try again");
    		return redirect()->back();
    	}
    }
}
