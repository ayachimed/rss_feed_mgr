<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Http\Requests;
use Session;
class CategoryController extends Controller
{
    public function post_category(Request $r)
    {
    	if($r->category){
    		$newCat = new Category();
    		$newCat->name= $r->category;
            if(count(Category::where('name',$r->category)->get()) != 0){
                Session::flash("Error","Category already exists");
                return redirect()->back();
            }
    		if($newCat->save()){
    			Session::flash("Success","Category successfully submitted");
    			return redirect()->route("dashboard");
    		}
    	}
    	Session::flash("Error","Error while submitting new Category try again");
    	return redirect()->back();
    }
}
