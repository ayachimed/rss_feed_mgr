<?php

namespace App\Http\Controllers;
use App\FeedUrl;
use App\Category;
use Illuminate\Http\Request;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application landingpage.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $urls = FeedUrl::all();
        $categories = Category::all();
        return view('admin_panel',[
                "urls" => $urls,
                "categories" => $categories
        ]);
    }


    public function show_settings(){
        return view('settings');
    }
}
