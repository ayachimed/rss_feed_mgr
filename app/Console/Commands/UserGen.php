<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
class UserGen extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generate new User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $username = "user_name";
        $password = "123123";
        $email = "a@b.c";
        $bool = User::create([
            'name' => $username,
            'email' => $email,
            'password' => bcrypt($password),
        ]);
        if($bool){
            $this->info("user created successfully[email:".$email.", password:".$password."]");
        }

    }
}
