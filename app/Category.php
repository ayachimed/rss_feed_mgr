<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function feedUrls()
    {
    	return $this->hasMany("App\FeedUrl");
    }
}
