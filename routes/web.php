<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::get('/',[
	"as" => 'landing_page', "uses" => 'FeedController@show_feeds_index'
]);
Route::get("/home",function(){
	return redirect()->route("dashboard");
});
Route::get('/dashboard', [
	"as" => "dashboard", "uses" => 'HomeController@index'
]);

Route::get("/admin/settings",[
	"as" => "settings", "uses" => "HomeController@show_settings", "after" => 'auth'
]);

Route::post("/password/update",[
	"as" => "update_admin_password" , "uses" => "UserController@change_password", "after" => "auth"
]);

Route::post("/admin/new/url",[
	"as" => "new_url","uses"=>"FeedController@post_url", "after" => "auth"
]);
Route::post("/admin/new/category",[
	"as" => "new_category","uses"=>"CategoryController@post_category", "after" => "auth"
]);

Route::get("/admin/url/delete/{id}",[
	"as" => "delete_url","uses"=>"FeedController@delete_url", "after" => "auth"
]);

Route::get("/admin/url/{id}/update",[
	"as" => "show_update_url","uses" => "FeedController@show_update_url_view", "after" => "auth"
]);

Route::post("/admin/url/update",[
	"as" => "update_url","uses" => "FeedController@update_url", "after" => "auth"
]);